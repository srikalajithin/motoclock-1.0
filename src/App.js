import './App.css';
import DeafaultLayout from './default/DeafaultLayout';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter as Router } from 'react-router-dom';


function App() {
  return (
    <Router>
      <userProvider>
        <div className="App">
          <DeafaultLayout/>
        </div>
    </userProvider>

    </Router>
    
    
  );
}

export default App;
