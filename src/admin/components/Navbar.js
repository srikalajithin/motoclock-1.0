import React from 'react'
import { Link,NavLink } from 'react-router-dom'

const Navbar = () => {
  return (
    <>
    {/* Main Sidebar Container */}
  <aside className="main-sidebar sidebar-dark-primary elevation-4">
  {/* Brand Logo */}
  <a href="index3.html" className="brand-link">
    <img
      src="dist/img/AdminLTELogo.png"
      alt="AdminLTE Logo"
      className="brand-image img-circle elevation-3"
      style={{ opacity: ".8" }}
    />
    <span className="brand-text font-weight-light">MOTOLOCK</span>
  </a>
  {/* Sidebar */}
  <div className="sidebar">
    {/* Sidebar user panel (optional) */}
    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
      <div className="image">
        <img
          src="dist/img/user2-160x160.jpg"
          className="img-circle elevation-2"
          alt="User Image"
        />
      </div>
      <div className="info">
        <a href="#" className="d-block">
          Alexander Pierce
        </a>
      </div>
    </div>
    {/* SidebarSearch Form */}
    <div className="form-inline">
      <div className="input-group" data-widget="sidebar-search">
        <input
          className="form-control form-control-sidebar"
          type="search"
          placeholder="Search"
          aria-label="Search"
        />
        <div className="input-group-append">
          <button className="btn btn-sidebar">
            <i className="fas fa-search fa-fw" />
          </button>
        </div>
      </div>
    </div>
    {/* Sidebar Menu */}
    <nav className="mt-2">
      <ul
        className="nav nav-pills nav-sidebar flex-column"
        data-widget="treeview"
        role="menu"
        data-accordion="false"
      >
        {/* Add icons to the links using the .nav-icon class
       with font-awesome or any other icon font library */}
    
       
       
        
       
     
   
        <li className="nav-item">
          <NavLink to="customers" className="nav-link">
            <i className="nav-icon far fa-user" />
            <p>Customers</p>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="orders" className="nav-link">
            <i className="nav-icon far fa-user" />
            <p>Orders</p>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="products" className="nav-link">
            <i className="nav-icon far fa-image" />
            <p>Products</p>
          </NavLink>
        </li><li className="nav-item">
          <NavLink to="categories" className="nav-link">
            <i className="nav-icon far fa-image" />
            <p>Categories</p>
          </NavLink>
        </li><li className="nav-item">
          <NavLink to="brands" className="nav-link">
            <i className="nav-icon far fa-image" />
            <p>Brands</p>
          </NavLink>
        </li><li className="nav-item">
          <NavLink to="suppliers" className="nav-link">
            <i className="nav-icon far fa-image" />
            <p>Suppliers</p>
          </NavLink>
        </li><li className="nav-item">
          <NavLink to="currencies" className="nav-link">
            <i className="nav-icon far fa-image" />
            <p>Currencies</p>
          </NavLink>
        </li><li className="nav-item">
          <NavLink to="languages" className="nav-link">
            <i className="nav-icon far fa-image" />
            <p>Languages</p>
          </NavLink>
        </li><li className="nav-item">
          <NavLink to="countries" className="nav-link">
            <i className="nav-icon far fa-user" />
            <p>Countries</p>
          </NavLink>
        </li>
        
       
          
        
           
        <li className="nav-item">
          
          <ul className="nav nav-treeview">
            <li className="nav-item">
              
              <ul className="nav nav-treeview">
                <li className="nav-item">
                  
                </li>
                <li className="nav-item">
                  
                </li>
                <li className="nav-item">
                  
                </li>
                <li className="nav-item">
                  
                </li>
              </ul>
            </li>
            <li className="nav-item">
              
              <ul className="nav nav-treeview">
                <li className="nav-item">
                  
                </li>
                <li className="nav-item">
                  
                </li>
                <li className="nav-item">
                  
                </li>
                <li className="nav-item">
                  
                </li>
              </ul>
            </li>
            <li className="nav-item">
             
            </li>
            <li className="nav-item">
             
            </li>
            <li className="nav-item">
             
            </li>
            
           
           
           
            
          </ul>
        </li>
        <li className="nav-item">
          
          <ul className="nav nav-treeview">
            <li className="nav-item">
              
            </li>
            <li className="nav-item">
              
            </li>
          </ul>
        </li>
        <li className="nav-header"></li>
        <li className="nav-item">
          
        </li>
        <li className="nav-item">
          
        </li>
        <li className="nav-header"></li>
        <li className="nav-item">
          
        </li>
        <li className="nav-item">
          
          <ul className="nav nav-treeview">
            <li className="nav-item">
             
            </li>
            <li className="nav-item">
              
              <ul className="nav nav-treeview">
                <li className="nav-item">
                 
                </li>
                <li className="nav-item">
                  
                </li>
                <li className="nav-item">
                  
                </li>
              </ul>
            </li>
            <li className="nav-item">
             
            </li>
          </ul>
        </li>
        <li className="nav-item">
          
        </li>
       
       
        
        
      </ul>
    </nav>
    {/* /.sidebar-menu */}
  </div>
  {/* /.sidebar */}
</aside>
</>

  )
}

export default Navbar