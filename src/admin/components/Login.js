import React,{useState,useContext} from 'react'
import { useNavigate } from "react-router-dom"
import { ToastContainer, toast } from 'react-toastify';
import Api from '../Api.js'
import { AppContext } from '../../default/AppContext.js';


const Login = () => {
    //context
    const {userToken,setToken} = useContext(AppContext)
    //useHistory
    const navigate = useNavigate();
    //useStates
    const [formValues,setFormValues] = useState({
        'username':'',
        'password':'',
        'remember_me':1
    })
    const [errUserName,setErrUserName]=useState('')
    const [errPassword,setErrPassword]=useState('')
    const [error,setError] = useState('')

    const handleError = () =>{
        validate()
    }
   
    const handleChange = (e) =>{
        setFormValues({...formValues,[e.target.name]:e.target.value})

    }
    
   
   // Login 
    const proceedLogin = async(e) =>{
        e.preventDefault();
        if(validate()){

            let response = await Api.post('login',formValues)
                if(response.error)
                {
                    setError(response.msg)
                    toast.error(error,{theme:'colored'})
                    return;
                }
                else{
                    localStorage.setItem('access_token',response.access_token)

                    setToken(response.access_token)
                    navigate('dashboard');
                    toast.success('Login successful',{theme:'colored'})
                }
        }
    }
    // const changeValue = () =>{
    //     setRememberMe(!rememberMe)
    // }
 // Validate
    const validate = () => {
        let result = true;
        if(formValues.password === '' || formValues.password === null)
        {
            setErrPassword('Password can not be blank!')
            result = false;
        }
        else
        if(formValues.userName === '' || formValues.userName === null)
        {
            setErrUserName('Username can not be blank!')
            result = false;
        }
        return result;
    }
  return (
    <>
            <div class="hold-transition login-page">
                <div className="login-box">
                    <div className="login-logo">
                    <a href="../../index2.html">
                        <b>Moto</b>Clock
                    </a>
                    </div>
                    {/* /.login-logo */}
                    <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Sign in to start your session</p>
                        <form onSubmit={proceedLogin}>
                        <div className="input-group mb-3">
                            <input type="text" 
                                   value={formValues.userName}
                                   name='username' 
                                   onChange={handleChange} 
                                   onBlur={handleError}
                                   className="form-control" 
                                   placeholder="User Name" />
                            <div className="input-group-append">
                            <div className="input-group-text">
                                <span className="fas fa-envelope" />
                            </div>
                            </div>
                        </div>
                        {errUserName &&
                            <div className="input-group mb-3">
                                 {errUserName}
                            </div>
                        }
                        <div className="input-group mb-3">
                            <input
                            type="password"
                            name='password'
                            onChange={handleChange} 
                            onBlur={handleError}
                            value={formValues.password}
                            className="form-control"
                            placeholder="Password"
                            />
                            <div className="input-group-append">
                            <div className="input-group-text">
                                <span className="fas fa-lock" />
                            </div>
                            </div>
                            {errPassword &&
                            <div className="input-group mb-3">
                                {errPassword}
                            </div>
                        }
                        </div>
                        <div className="row">
                            <div className="col-8">
                            <div className="icheck-primary">
                                <input type="checkbox" id="remember" value={formValues.remember_me}/>
                                <label htmlFor="remember">Remember Me</label>
                            </div>
                            </div>
                            {/* /.col */}
                            <div className="col-4">
                            <button type="submit" className="btn btn-primary btn-block">
                                Sign In
                            </button>
                            </div>
                            {/* /.col */}
                        </div>
                        </form>
            </div>
            {/* /.login-card-body */}
                    </div>
                </div>
                {/* /.login-box */}
            </div>
            <ToastContainer/>
    </>
  )
}

export default Login