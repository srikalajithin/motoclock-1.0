import config from "../config";

const apiURL = config.api_admin.url; // ADMIN API URL
const userToken = localStorage.getItem('access_token')
console.log(userToken,'userToken')
// Common function for making POST requests
 const Api =
 {
      post : async (url, data) => {
      try {
          let result = await fetch(`${apiURL}/${url}`, {
            method: 'POST',
            headers:{
                      'Content-Type':'application/json',
                      'accept':'application/json',
                  },
            body: JSON.stringify(data),
        });
         result =  await result.json()
        console.log(result,'result')
        return result;
      } catch (error) {
        throw error;
      }
    },
    get : async(url,id=null)=>{
      let fetchUrl = id==null ? `${apiURL}/${url}` : `${apiURL}/${url}/${id}`
      console.log(fetchUrl,'fetchurl')
      try{
          let result = await fetch(fetchUrl,{
            method:'GET',
            headers:{
              'Content-Type':'application/json',
              'accept':'application/json',
              'Authorization':`Bearer ${userToken}`
          },
        })
        result =  await result.json()
        console.log(result,'result')
        return result;
      }
      catch(error){
        throw(error)
      }
    }
}



export default Api