import React,{ createContext, useContext ,useState} from 'react'
import {Route,Outlet } from 'react-router-dom';
import Footer from './components/Footer'
import Header from './components/Header'
import Navbar from './components/Navbar'
import Home from './pages/Home'
import Login from './components/Login';
import { AppContext } from '../default/AppContext';

const AdminLayout = () => {
  //setting usertoken
  const [userToken,setUserToken] = useState(null) 
  const setToken = (token) => {
      setUserToken(token)
      console.log(token,'token from default')
  }  

  return (
    <AppContext.Provider value={{userToken,setToken}}>
      {userToken ? 
        <div>
          <Header/>
          <Outlet/>
          <Navbar/>
          <Footer/>

      </div> 
      :
      <Login/>
    }
    </AppContext.Provider>
  )
}

export default AdminLayout