import React, { useEffect, useState } from 'react';
import Api from '../Api.js';
import { NavLink } from 'react-router-dom';

const Suppliers = () => {
  const [suppliers,setSuppliers] = useState([])
  useEffect(()=>{
    async function getCategories(){

      try {
        const response = await Api.get('supplieres');

        if (response && response.data) {

          setSuppliers(response.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    getCategories()

  },[]);
  // Add a useEffect to log the updated categories state
  useEffect(() => {
    console.log(suppliers, 'Updated categories');
    suppliers.map((supplier)=>{
      console.log(supplier,'supplier')
    })
  }, [suppliers]);
  return (
<>
  <div className="content-wrapper">
      <div className="col-12">
        <div className="card">
          <div className="card-header" style={{ height: 100 }}>
            <div style={{ marginRight: 26 }}>
              <h2 className="card-title text-primary" style={{ marginTop: 10 }}>
                Suppliers
              </h2>
            </div>
            <div className="card-tools">
              <button style={{ marginRight: 26 }} className="btn btn-success">
                Add
              </button>
              <ul className="pagination pagination-sm float-right">
                <li className="page-item">
                  <a className="page-link" href="#">
                    «
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    »
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/* /.card-header */}
          <div className="card-body p-0">
            <table className="table">
              <thead>
                <tr>
                <th>#</th>
                  <th>Supplier Id</th>
                  <th>Supplier Name</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {suppliers.map((supplier,index) => (
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{supplier.id}</td>
                    <td>{supplier.name}</td>
                    <td>
                      <span>
                        <button
                          style={{ margin: 10 }}
                          className="btn btn-success"
                        >
                          View
                        </button>
                      </span>
                      <span>
                        <button style={{ margin: 10 }} className="btn btn-primary">
                          Edit
                        </button>
                      </span>
                      <span>
                        <button className="btn btn-danger">Delete</button>
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          {/* /.card-body */}
        </div>
        {/* /.card */}
      </div>
    </div>
    </>  )
}

export default Suppliers