import React, { useEffect, useState } from 'react';
import Api from '../Api.js';
import { NavLink } from 'react-router-dom';

const Products = () => {
  const [products,setProducts] = useState([])
  useEffect(()=>{
    async function getProducts(){

      try {
        const response = await Api.get('products');

        if (response && response.data) {

          setProducts(response.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    getProducts()

  },[]);
  // Add a useEffect to log the updated products state
  useEffect(() => {
    console.log(products, 'Updated products state');
    products.map((product)=>{
      console.log(product,'singleprodcuct')
    })
  }, [products]);
  return (
    <>
  <div className="content-wrapper">
      <div className="col-12">
        <div className="card">
          <div className="card-header" style={{ height: 100 }}>
            <div style={{ marginRight: 26 }}>
              <h2 className="card-title text-primary" style={{ marginTop: 10 }}>
                Products
              </h2>
            </div>
            <div className="card-tools">
              <button style={{ marginRight: 26 }} className="btn btn-success">
                Add
              </button>
              <ul className="pagination pagination-sm float-right">
                <li className="page-item">
                  <a className="page-link" href="#">
                    «
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    »
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/* /.card-header */}
          <div className="card-body p-0">
            <table className="table">
              <thead>
                <tr>
                <th style={{ width: 10 }}>#</th>
                  <th style={{ width: 20 }}>Id</th>
                  <th>Name</th>
                  <th>Descriptions</th>
                  <th>SKU</th>
                  <th>Price</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {products.map((product,index) => (
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{product.descriptions[0].product_id}</td>
                    <td>{product.descriptions[0].name}</td>
                    <td>{product.descriptions[0].description}</td>

                    <td>{product.sku}</td>
                    <td>{product.price}</td>
                    <td>
                      <span>
                        <button
                          style={{ margin: 10 }}
                          className="btn btn-success"
                        >
                          View
                        </button>
                      </span>
                      <span>
                        <button style={{ margin: 10 }} className="btn btn-primary">
                          Edit
                        </button>
                      </span>
                      <span>
                        <button className="btn btn-danger">Delete</button>
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          {/* /.card-body */}
        </div>
        {/* /.card */}
      </div>
    </div>
    </>
  );
}

export default Products