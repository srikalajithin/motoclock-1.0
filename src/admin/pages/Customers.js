import React, { useEffect, useState } from 'react';
import Api from '../Api.js';
import { NavLink } from 'react-router-dom';
import CustomerModal from '../modals/CustomerModal.js';

const Customers = () => {
  const [customers, setCustomers] = useState([]);
  const [show, setShow] = useState(false);
  const [customerId, setCustomerId] = useState(null);

  const handleClose = () => setShow(false);
  const handleShow = (id) => {
    console.log(id, 'id');
    setCustomerId(id); // Set the customerId
    setShow(true);
  };

  useEffect(() => {
    async function getCustomerDetails() {
      try {
        const response = await Api.get('customers');
        if (response && response.data) {
          setCustomers(response.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    getCustomerDetails();
  }, []);

  return (
    <>
     <div className="content-wrapper">
     <div className="col-12">
        <div className="card">
          <div className="card-header" style={{ height: 100 }}>
            <div style={{ marginRight: 26 }}>
              <h2 className="card-title text-primary" style={{ marginTop: 10 }}>
                Customers
              </h2>
            </div>
            <div className="card-tools">
              <button style={{ marginRight: 26 }} className="btn btn-success">
                Add
              </button>
              <ul className="pagination pagination-sm float-right">
                <li className="page-item">
                  <a className="page-link" href="#">
                    «
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    »
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/* /.card-header */}
          <div className="card-body p-0">
            <table className="table">
              <thead>
                <tr>
                  <th style={{ width: 20 }}>Id</th>
                  <th style={{ width: 40 }}>Name</th>
                  <th style={{ width: 30 }}>Email</th>
                  <th>Mobile</th>
                  <th style={{ width: 60 }}>Address</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {customers.map((customer) => (
                  <tr key={customer.id}>
                    <td>{customer.id}</td>
                    <td>{customer.name}</td>
                    <td>{customer.email}</td>
                    <td>{customer.phone}</td>
                    <td>{customer.address1}</td>
                    <td>
                      <span>
                        <button
                          style={{ margin: 10 }}
                          className="btn btn-success"
                          onClick={() => handleShow(customer.id)}
                        >
                          View
                        </button>
                      </span>
                      <span>
                        <button style={{ margin: 10 }} className="btn btn-primary">
                          Edit
                        </button>
                      </span>
                      <span>
                        <button className="btn btn-danger">Delete</button>
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          {/* /.card-body */}
        </div>
        {/* /.card */}
      </div>
     </div>
      
      <CustomerModal show={show} onClose={handleClose} customerId={customerId} />
    </>
  );
};

export default Customers;
