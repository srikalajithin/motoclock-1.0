import React, { useEffect, useState } from 'react';
import Api from '../Api.js';
import { NavLink } from 'react-router-dom';
const Brands = () => {
  const [brands,setBrands] = useState([])
  useEffect(()=>{
    async function getBrands(){

      try {
        const response = await Api.get('brands');

        if (response && response.data) {

          setBrands(response.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    getBrands()

  },[]);
  // Add a useEffect to log the updated categories state
  useEffect(() => {
    console.log(brands, 'Updated categories');
    brands.map((brand)=>{
      console.log(brand,'category')
    })
  }, [brands]);
  return (
    <>
     <div className="content-wrapper">
     <div className="col-12">
        <div className="card">
          <div className="card-header" style={{ height: 100 }}>
            <div style={{ marginRight: 26 }}>
              <h2 className="card-title text-primary" style={{ marginTop: 10 }}>
                Brands
              </h2>
            </div>
            <div className="card-tools">
              <button style={{ marginRight: 26 }} className="btn btn-success">
                Add
              </button>
              <ul className="pagination pagination-sm float-right">
                <li className="page-item">
                  <a className="page-link" href="#">
                    «
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    »
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/* /.card-header */}
          <div className="card-body p-0">
            <table className="table">
              <thead>
                <tr>
                <th>#</th>
                  <th>Brand Id</th>
                  <th>Brand Name</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {brands.map((brand,index) => (
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{brand.id}</td>
                    <td>{brand.name}</td>
                    <td>
                      <span>
                        <button
                          style={{ margin: 10 }}
                          className="btn btn-success"
                        >
                          View
                        </button>
                      </span>
                      <span>
                        <button style={{ margin: 10 }} className="btn btn-primary">
                          Edit
                        </button>
                      </span>
                      <span>
                        <button className="btn btn-danger">Delete</button>
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          {/* /.card-body */}
        </div>
        {/* /.card */}
      </div>
     </div>
      
    </>
  )
}

export default Brands