import React,{useEffect,useState} from 'react'
import Api from '../Api'
import { NavLink } from 'react-router-dom';


const Orders = () => {

  //useStates

  const [orders,setOrders] = useState([])

  //useEffect

  useEffect(()=>{
    async function getOrders(){
      try{
        const responce = await Api.get('orders')
        if (responce && responce.data) {
             setOrders(responce.data);
        }
      }
      catch (error) {
        console.error(error);
      }
    }
    getOrders()
  },[])

  return (
    <>
    <div className="content-wrapper">
       <div className="col-12">
        <div className="card">
          <div className="card-header" style={{ height: 100 }}>
            <div style={{ marginRight: 26 }}>
              <h2 className="card-title text-primary" style={{ marginTop: 10 }}>
               Orders
              </h2>
            </div>
            <div className="card-tools">
              <button style={{ marginRight: 26 }} className="btn btn-success">
                Add
              </button>
              <ul className="pagination pagination-sm float-right">
                <li className="page-item">
                  <a className="page-link" href="#">
                    «
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    »
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/* /.card-header */}
          <div className="card-body p-0">
            <table className="table">
              <thead>
                <tr>
                  <th style={{ width: 20 }}>Id</th>
                  <th style={{ width: 40 }}>Customer Name</th>
                  <th style={{ width: 30 }}>Mobile</th>
                  <th>Email</th>
                  <th style={{ width: 60 }}>Address</th>
                  <th style={{ width: 60 }}>Payment Method</th>
                  <th style={{ width: 60 }}>Shipping Method</th>
                  <th style={{ width: 60 }}>Order Date</th>
                  <th style={{ width: 60 }}>Modified Date</th>
                  <th style={{ width: 60 }}>Sub Total</th>
                  <th style={{ width: 60 }}>Tax</th>
                  <th style={{ width: 60 }}>Grand Total</th>


                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {orders && orders.map((order) => (
                  <tr key={order.id}>
                    <td>{order.id}</td>
                    <td>{order.name}</td>
                    <td>{order.phone}</td>
                    <td>{order.email}</td>
                    <td>{`${order.address1},${order.address1}`}</td>
                    <td>{order.payment_method}</td>
                    <td>{order.shipping_method}</td>
                    <td>{order.created_at}</td>
                    <td>{order.updated_at}</td>
                    <td>{order.subtotal}</td>
                    <td>{order.tax}</td>
                    <td>{order.total}</td>


                    <td>
                      <span>
                        <button
                          style={{ margin: 10 }}
                          className="btn btn-success"
                        >
                          View
                        </button>
                      </span>
                      <span>
                        <button style={{ margin: 10 }} className="btn btn-primary">
                          Edit
                        </button>
                      </span>
                      <span>
                        <button className="btn btn-danger">Delete</button>
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          {/* /.card-body */}
        </div>
        {/* /.card */}
      </div>
      </div>
    </>
  )
}

export default Orders