import React, { useEffect, useState } from 'react';
import Api from '../Api.js';
import { NavLink } from 'react-router-dom';

const Currencies = () => {
  const [currencies,setCurrencies] = useState([])
  useEffect(()=>{
    async function getBrands(){

      try {
        const response = await Api.get('currencies');

        if (response && response.data) {

          setCurrencies(response.data);
        }
      } catch (error) {
        console.error(error);
      }
    }
    getBrands()

  },[]);
  useEffect(() => {
    console.log(currencies, 'Updated categories');
    currencies.map((currency)=>{
      console.log(currency,'currencies')
    })
  }, [currencies]);
  
  return (
    <>
        <div className="content-wrapper">
        <div className="col-12">
      <div className="card">
        <div className="card-header" style={{ height: 100 }}>
          <div style={{ marginRight: 26 }}>
            <h2 className="card-title text-primary" style={{ marginTop: 10 }}>
              Currencies
            </h2>
          </div>
          <div className="card-tools">
            <button style={{ marginRight: 26 }} className="btn btn-success">
              Add
            </button>
            <ul className="pagination pagination-sm float-right">
              <li className="page-item">
                <a className="page-link" href="#">
                  «
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  1
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  2
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  3
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  »
                </a>
              </li>
            </ul>
          </div>
        </div>
        {/* /.card-header */}
        <div className="card-body p-0">
          <table className="table">
            <thead>
              <tr>
              <th>#</th>
                <th>Currency Id</th>
                <th>Currency Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {currencies.map((currency,index) => (
                <tr key={index}>
                  <td>{index+1}</td>
                  <td>{currency.id}</td>
                  <td>{currency.name}</td>
                  <td>
                    <span>
                      <button
                        style={{ margin: 10 }}
                        className="btn btn-success"
                      >
                        View
                      </button>
                    </span>
                    <span>
                      <button style={{ margin: 10 }} className="btn btn-primary">
                        Edit
                      </button>
                    </span>
                    <span>
                      <button className="btn btn-danger">Delete</button>
                    </span>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        {/* /.card-body */}
      </div>
      {/* /.card */}
    </div>
        </div>

   
  </>

  )
}

export default Currencies