import React,{useEffect,useState} from 'react'
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Api from '../Api';

const CustomerModal = (props) => {
    //useState
    const [customer,setcustomer] = useState(null)
    //props destructuring..
    const {show,onClose,customerId} = props
    console.log('customerId',customerId)

    // api call for getting customer data
    const getCustomer = async() =>{
        try {
            const responce = await Api.get('customers',customerId)
            if (responce) {
                setcustomer(responce);
                console.log('customer',customer)
              }
        } catch (error) {
            console.error('Error:', error);
          }
    }
   
        show && getCustomer()
       

  return (
    <div
        className="modal show"
        style={{ display: 'block', position: 'initial' }}
    >
        <Modal show={show} onHide={onClose}>
        <Modal.Header closeButton>
            <Modal.Title>Customer Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div class="row">
            <div class="col">Name</div>
            <div class="col">{customer && customer.name}</div>
            <div class="w-100"></div>
        </div>
        <div class="row">
            <div class="col">First Name</div>
            <div class="col">{customer && customer.first_name}</div>
            <div class="w-100"></div>
        </div>
        <div class="row">
            <div class="col">Last Name</div>
            <div class="col">{customer && customer.last_name}</div>
            <div class="w-100"></div>
        </div>
        <div class="row">
            <div class="col">Email</div>
            <div class="col">{customer && customer.email}</div>
            <div class="w-100"></div>
        </div>
        <div class="row">
            <div class="col">Mobile</div>
            <div class="col">{customer && customer.phone}</div>
            <div class="w-100"></div>
        </div>
        <div class="row">
            <div class="col">Address</div>
            <div class="col">{customer && `${customer.address1},${customer.address2}`}</div>
            <div class="w-100"></div>
        </div>
        
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onClose}>Close</Button>
        </Modal.Footer>
        </Modal>
    </div>
  )
}

export default CustomerModal