import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AdminLayout from '../admin/AdminLayout';
import Customers from '../admin/pages/Customers';
import Orders from '../admin/pages/Orders';
import Categories from '../admin/pages/Categories';
import Products from '../admin/pages/Products';
import Brands from '../admin/pages/Brands';
import Countries from '../admin/pages/Countries';
import Languages from '../admin/pages/Languages';
import Suppliers from '../admin/pages/Suppliers';
import Currencies from '../admin/pages/Currencies';
import Home from "../admin/pages/Home";


const DeafaultLayout = () => {

  
  return (
    <>
    <AdminLayout />
    <Routes>
        <Route path="/admin" element={<Home />}/>
          <Route index element={<Home />}/>
          <Route path="dashboard" element={ <Home /> }/>
          <Route path="customers" element={ <Customers /> }/>
          <Route path="orders" element={ <Orders /> }/>
          <Route path="products" element={<Products /> }/>
          <Route path="categories" element={ <Categories /> }/>
          <Route path="brands" element={ <Brands /> }/>
          <Route path="suppliers" element={ <Suppliers /> }/>
          <Route path="currencies" element={ <Currencies /> }/>
          <Route path="languages" element={ <Languages /> }/>
          <Route path="countries" element={ <Countries /> }/>
    </Routes>
    </>
    
  )
}

export default DeafaultLayout