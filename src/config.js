const config = {
    api_admin:{
        url: process.env.REACT_APP_API_URL || null
    }
}

export default config;